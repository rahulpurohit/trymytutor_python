from django.db import models

# Create your models here.
##for custom auth User
from django.conf import settings
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _
import geocoder
# from django.contrib.postgres.fields import ArrayField
import cloudinary
import cloudinary.uploader
import cloudinary.api
## Trying to Convert Image   
from django.core.files.base import ContentFile
import base64
import six
import uuid
import imghdr
##



def image_convert(data_base64):
    print("Trying to Convert BAse64 to Image")
    data = data_base64
    # Check if this is a base64 string
    if isinstance(data, six.string_types):
        # Check if the base64 string is in the "data:" format
        if 'data:' in data and ';base64,' in data:
            # Break out the header from the base64 content
            header, data = data.split(';base64,')

        # Try to decode the file. Return validation error if it fails.
        try:
            decoded_file = base64.b64decode(data)
        except TypeError:
            self.fail('invalid image')

        # Generate file name:
        file_name = str(uuid.uuid4())[:12] # 12 characters are more than enough.
        # Get the file name extension:
        extension = imghdr.what(file_name, decoded_file)
        extension = "png" if extension == "jpg" else extension
        file_extension = extension
        complete_file_name = "%s.%s" % (file_name, file_extension, )
        data = ContentFile(decoded_file, name=complete_file_name)
        return complete_file_name


def trying(instance):
    print("Trying Function")
    return "%s" % (instance.id)

def upload_location(instance,filename):
    print("Trying Function of upload_location")
    UserModel = instance.__class__
    new_id = UserModel.objects.order_by("id").last().id + 1
    return "user/%s/%s" %(new_id,filename)

class Role(models.Model):
	# models.ForeignKey(settings.AUTH_USER_MODEL)
	name = models.CharField(max_length=30,blank=True)
	created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
	updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

	def __str__(self):
		return "%s " % self.name

class SubjectCategory(models.Model):
    name = models.CharField(max_length=30,blank=True)
    cat_type = models.CharField(max_length=30,blank=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

    def __str__(self):
        return "%s " % self.name



class SubSubjectCategory(models.Model):
    subject_cat_id = models.ForeignKey(SubjectCategory,on_delete=models.CASCADE)
    name = models.CharField(max_length=30,blank=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

    def __str__(self):
        return "%s " % self.name


class City(models.Model):
    name = models.CharField(max_length=30,blank=True)
    detail = models.CharField(max_length=30,blank=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

    def __str__(self):
        return "%s " % self.name

class CityZone(models.Model):
    city_id = models.ForeignKey(City,on_delete=models.CASCADE)
    name = models.CharField(max_length=30,blank=True)
    detail = models.CharField(max_length=30,blank=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

    def __str__(self):
        return "%s " % self.name

class Area(models.Model):
    city_id = models.ForeignKey(City,on_delete=models.CASCADE)
    city_zone_id = models.ForeignKey(CityZone,on_delete=models.CASCADE)
    name = models.CharField(max_length=30,blank=True)
    detail = models.CharField(max_length=30,blank=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

    def __str__(self):
        return "%s " % self.name


class Board(models.Model):
    name = models.CharField(max_length=30,blank=True)
    detail = models.CharField(max_length=30,blank=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

    def __str__(self):
        return "%s " % self.name

class MyUserManager(BaseUserManager):
    def _create_superuser(self, email, password, is_admin, is_superuser, role_id_id,**extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        
        if not email:
            raise ValueError('The given email must be set')

        email = self.normalize_email(email.lower())

        user = self.model(email=email,
                          is_admin=is_admin, is_active=True,
                           **extra_fields)
        user.role_id_id = role_id_id
        user.set_password(password)
        user.save(using=self._db)
        return user
    def _create_user(self, email, password, is_admin, is_superuser,**extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')

        email = self.normalize_email(email.lower())

        user = self.model(email=email,
                          is_admin=is_admin, is_active=True,
                           **extra_fields)
        user.set_password(password)
        print(user)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        try:

            return self._create_user(email, password, False, False,**extra_fields)
        except:
            print("Errr")


    def create_superuser(self, email, password, **extra_fields):
        return self._create_superuser(email, password, True, True, 1,**extra_fields)


class MyUser(AbstractBaseUser):
    email = models.EmailField(_('email address'),
        max_length=255,
        unique=True,
    )
    first_name = models.CharField(_('first name'),max_length=30,blank=True)
    last_name = models.CharField(_('last name'),max_length=30,blank=True)
    age = models.IntegerField(null=True, blank=True)
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = models.CharField(_('gender'),max_length=1, choices=GENDER_CHOICES,blank=True)
    address = models.CharField(_('address'),max_length=150,blank=True)
    lat = models.FloatField(_('latitude'),default=0, blank=True)
    lng = models.FloatField(_('longitude'),default=0, blank=True)
    phone_no = models.CharField(_('phone number'),max_length=30,blank=True)
    description = models.CharField(_('description'),max_length=300,blank=True)
    role_id = models.ForeignKey(Role,on_delete=models.CASCADE)
    # cat_id = models.ForeignKey(Category,on_delete=models.CASCADE,null=True,blank=True)
    image = models.ImageField(upload_to=upload_location,null=True,blank=True)
    user_cv = models.FileField(upload_to=upload_location,null=True,blank=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)
    is_active = models.BooleanField(_('is_active'),default=True)
    is_admin = models.BooleanField(_('is_admin'),default=False)
    is_staff = models.BooleanField(_('is_staff'),default=False)
    online_status = models.BooleanField(_('online_status'),default=False)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name']

    class Meta:
        verbose_name = ('user')
        verbose_name_plural = _('users')
        # abstract = True

    def get_absolute_url(self):
        return "/users/%s" % urlquote(self.email)

    def get_full_name(self):
        # The user is identified by their email address
        full_name = '%s %s' % (self.first_name,self.last_name)
        return '%s %s' % (self.first_name,self.last_name)

    def full_name(self):
        return '%s %s' % (self.first_name,self.last_name)

    def get_short_name(self):
        # The user is identified by their email address
        return self.first_name

    def __str__(self):              # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    def email_user(self,subject,message,from_email=None):
        """
        Sends an email to this User
        """
        send_mail(subject, message,from_email,[self.email])

    def add_category(self, category):
        print("In Add Category")
        relationship, created = Users_Subject.objects.get_or_create(user_id=self,cat_id=category)
        print("Category Added %s " % relationship)
        return relationship    

    def remove_category(self, category):
        Users_Subject.objects.filter(user_id=self,cat_id=category).delete()
        print("Category is Delete From User")
        return

    # def all_category(self):
    #     all = self.category_relation.all()
    #     return all

    def update_category(self,updated_list):
        user_cat = Users_Subject.objects.filter(user_id=self)
        print(user_cat)
        current_cat_list = []
        if user_cat:
            for i in range(0,len(user_cat)):
                current_cat_list.append(user_cat[i].cat_id_id)
            current_setA = set(current_cat_list)
            update_setB = set(updated_list)

            delete_set = current_setA - update_setB
            update_set = update_setB - current_setA
            for id in delete_set:
                deleted = Users_Subject.objects.filter(user_id=self,cat_id_id=id).delete()
                print("DElete this id in cat Table %s" % id)
            for id in update_set:
                updated = Users_Subject.objects.create(user_id=self,cat_id_id=id)
                print("Update or Create this id in cat Table %s" % id)
        else:
            for i in range(0,len(updated_list)):
                updated = Users_Subject.objects.create(user_id=self,cat_id_id=updated_list[i])
                print("Update or Create this id in cat Table %s" % updated_list[i])
        return updated_list



    
    

    def save(self, *args, **kwargs):
        """
        Use the pygments libary to create a highlighted HTML
        Representation of the code snippet.
        """

        # g = geocoder.google(self.address)
        # latlng = g.latlng
        # print(latlng)
        # self.lat = g.latlng[0]
        # self.lng = g.latlng[1]
        super(MyUser,self).save( *args, **kwargs)        

    

    def image_tag(self):
        img = '<img src="%s" />' % self.image
        return '<img src="%s"/>' % self.image
        image_tag.short_description = 'Image'
        image_tag.allow_tags = True   

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    def _get_image_url(self):
        if self.image:
            return self.image.url
        else:
            return None
    image_url = property(_get_image_url)




class UserTutorPreference(models.Model):
    user_id = models.ForeignKey(MyUser,on_delete=models.CASCADE)
    tutor_detail = models.CharField(max_length=30,blank=True)
    teaching_exp = models.CharField(max_length=30,blank=True)
    TIME_SLOT_CHOICES = (
        ('M', 'Morning'),
        ('A', 'Afternoon'),
        ('E', 'Evening'),
    )
    prefer_time_slot = models.CharField(_('prefer_time_slot'),max_length=1, choices=TIME_SLOT_CHOICES,blank=True)
    TUTION_TYPE_CHOICES = (
        ('O', 'ONE-ON-ONE'),
        ('G', 'GROUP'),

    )
    prefer_tution_type = models.CharField(_('prefer_time_slot'),max_length=1, choices=TIME_SLOT_CHOICES,blank=True)
    prefer_duration_session = models.CharField(max_length=30,blank=True)
    Mon = models.BooleanField(_('Mon'),default=False)
    Tue = models.BooleanField(_('Tue'),default=False)
    Wed = models.BooleanField(_('Wed'),default=False)
    Thu = models.BooleanField(_('Thu'),default=False)
    Fri = models.BooleanField(_('Fri'),default=False)
    Sat = models.BooleanField(_('Sat'),default=False)
    Sun = models.BooleanField(_('Sun'),default=False)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

    def __str__(self):
        return "%s " % self.id    

class University(models.Model):
    name = models.CharField(max_length=30,blank=True)
    address = models.CharField(_('address'),max_length=150,blank=True)
    lat = models.FloatField(_('latitude'),default=0, blank=True)
    lng = models.FloatField(_('longitude'),default=0, blank=True)
    area_id = models.ForeignKey(Area,on_delete=models.CASCADE)
    board_id = models.ForeignKey(Board,on_delete=models.CASCADE)
    pincode = models.CharField(_('pincode'),max_length=150,blank=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

    def __str__(self):
        return "%s " % self.name

class SocialAuth(models.Model):
    provider = models.CharField(max_length=30)
    u_id = models.CharField(max_length=191)
    user_id = models.ForeignKey(MyUser,on_delete=models.CASCADE)
    extra_data = models.CharField(max_length=30,blank=True,default='')
    last_login = models.DateTimeField(_('last_login'),auto_now = True,auto_now_add=False)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

class Rating(models.Model):
    feedback = models.CharField(max_length=30,blank=True)
    RATE_CHOICES = (
        (0,0),
        (1,1),
        (2,2),
        (3,3),
        (4,4),
        (5,5),
    )
    punctuality = models.IntegerField(_('punctuality'), choices=RATE_CHOICES)
    communication = models.IntegerField(_('communication'), choices=RATE_CHOICES)
    confidence = models.IntegerField(_('confidence'), choices=RATE_CHOICES)
    dress_up = models.IntegerField(_('dress_up'), choices=RATE_CHOICES)
    english_fluency = models.IntegerField(_('english_fluency'), choices=RATE_CHOICES)
    satisfaction = models.IntegerField(_('satisfaction'), choices=RATE_CHOICES)
    # appoint_id = models.ForeignKey(Appoint,on_delete=models.CASCADE)
    user_id = models.ForeignKey(MyUser,on_delete=models.CASCADE,related_name='user_rating_id')
    student_rate_id = models.ForeignKey(MyUser,on_delete=models.CASCADE,related_name='rate_student_id')
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

class Static_Content(models.Model):
    terms_conditions = models.TextField(_('terms_conditions'),blank=True) 
    about_us = models.TextField(_('about_us'),blank=True)
    pirvacy_policy = models.TextField(_('pirvacy_policy'),blank=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)





class Users_Subject(models.Model):
    r_name = models.CharField(max_length=30,blank=True)
    user_id = models.ForeignKey(MyUser,on_delete=models.CASCADE,related_name='user_id_categories')
    subject_id = models.ForeignKey(SubjectCategory,on_delete=models.CASCADE,related_name='subject_id_categories')
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)


    def __str__(self):
        return "user_id is %s , subject_id %s" % (self.user_id,self.subject_id)


# class Users_Sub_Subject(models.Model):
#     r_name = models.CharField(max_length=30,blank=True)
#     user_id = models.ForeignKey(MyUser,on_delete=models.CASCADE,related_name='user_id_categories')
#     subject_sub_id = models.ForeignKey(SubSubjectCategory,on_delete=models.CASCADE,related_name='subject_sub_id_categories')

#     def __str__(self):
#         return "user_id is %s , subject_sub_id %s" % (self.user_id,self.subject_sub_id)

############Update
class Expertise_Level(models.Model):
    name = models.CharField(max_length=30,blank=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)


class Subject_Expertise_Level(models.Model):
    subject_id = models.ForeignKey(SubjectCategory,on_delete=models.CASCADE)
    sub_subject_id = models.ForeignKey(SubSubjectCategory,on_delete=models.CASCADE)
    expertise_level = models.ForeignKey(Expertise_Level,on_delete=models.CASCADE)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)
##
class Tutor_Subject(models.Model):
    user_id = models.ForeignKey(MyUser,on_delete=models.CASCADE)
    subject_id = models.ForeignKey(SubjectCategory,on_delete=models.CASCADE)
    sub_subject_id = models.ForeignKey(SubSubjectCategory,on_delete=models.CASCADE)
    expertise_level = models.ForeignKey(Subject_Expertise_Level,on_delete=models.CASCADE)
    test_status = models.BooleanField(default=False)
    is_passed = models.CharField(max_length=30,blank=True)
    LEVEL_CHOICES = (
        ('Newbie','Newbie'),
        ('Novice','Novice'),
        ('Rookie','Rookie'),
        ('Beginner','Beginner'),
        ('Talented','Talented'),
        ('Skilled','Skilled'),
        ('Intermediate','Intermediate'),
        ('Skillful','Skillful'),
        ('Seasoned','Seasoned'),
        ('Proficient','Proficient'),
        ('Experienced','Experienced'),
        ('Advanced','Advanced'),
        ('Senior','Senior'),
        ('Expert','Expert')
    )
    pass_level = models.CharField(max_length=1, choices=LEVEL_CHOICES,blank=True)
    experty_measure = models.CharField(max_length=30,blank=True)
    personal_comment = models.CharField(max_length=30,blank=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)


class Tutor_Area(models.Model):
    user_id = models.ForeignKey(MyUser,on_delete=models.CASCADE)
    area_id = models.ForeignKey(Area,on_delete=models.CASCADE)
    is_passed = models.CharField(max_length=30,blank=True)
    personal_comment = models.CharField(max_length=30,blank=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)


class User_Qualification(models.Model):
    user_id = models.ForeignKey(MyUser,on_delete=models.CASCADE)
    degree_name = models.CharField(max_length=30)
    start_year = models.DateField()
    complete_year = models.DateField()
    university_name = models.CharField(max_length=30)
    percentage = models.CharField(max_length=30)
    is_completed = models.BooleanField(_('is_completed'),default=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)




    