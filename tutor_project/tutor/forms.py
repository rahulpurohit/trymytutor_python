
from django import forms
from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)
from django.utils.translation import ugettext_lazy as _
from tutor.models import *
import geocoder
import re
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist


class RegistrationForm(forms.Form):
    username = forms.CharField(label='Username', max_length=30)
    email = forms.EmailField(label='Email')
    password1 = forms.CharField(label='Password',
                          widget=forms.PasswordInput())
    password2 = forms.CharField(label='Password (Again)',
                        widget=forms.PasswordInput())

    def clean_password2(self):
        if 'password1' in self.cleaned_data:
            password1 = self.cleaned_data['password1']
            password2 = self.cleaned_data['password2']
            if password1 == password2:
                return password2
        raise forms.ValidationError('Passwords do not match.')

     # def clean_username(self):
     #    username = self.cleaned_data['username']
     #    if not re.search(r'^\w+$', username):
     #        raise forms.ValidationError('Username can only contain alphanumeric characters and the underscore.')
     #    try:
     #        MyUser.objects.get(username=username)
     #    except ObjectDoesNotExist:
     #        return username
     #    raise forms.ValidationError('Username is already taken.')    


class UserForm(forms.ModelForm):
    email = forms.EmailField(required = True)
    first_name = forms.CharField(required = True)
    last_name = forms.CharField(required = False)
    password = forms.CharField(required = True)
    # address = forms.CharField(required = True)
    # gender = forms.CharField(required = True)
    age = forms.IntegerField(required = True)
    # pincode = forms.IntegerField(required = True)
    role_id = forms.CharField(required = True)#forms.ModelChoiceField(queryset=Role.objects.get(pk=2),widget=forms.HiddenInput())
    

    # def clean_age(self):
    #     age = self.cleaned_data['age']
    #     # today = date.today()
    #     if age <= 17:
    #         raise forms.ValidationError('Must be at least 18 years old to register')
    #     elif age >= 80:
    #         raise forms.ValidationError('Must be less than 99 years old to register')
    #     return age
    def clean_role_id(self):
      role_id = self.cleaned_data['role_id']
      return Role.objects.get(pk=role_id)
    # #   # if "/nurse/register/" in request.build_absolute_uri():
    # #   #     return Role.objects.get(name="Nurse")
    # #   # if "/patient/register/" in request.build_absolute_uri():
    # #   return 
    # def clean_address(self):
    #   address = self.cleaned_data['address']
    #   try:
    #     g = geocoder.google(self.cleaned_data['address'])
    #     lat = g.latlng[0]
    #     lng = g.latlng[1]
    #     return address
    #   except Exception as e:
    #       raise forms.ValidationError('Please Enter Address Correctly')

    class Meta:
        model = MyUser
        #fields = ('email', 'password', 'first_name','address','lat','lng','gender','role_id','age','description','phone_no','image', 'user_cv')        
        exclude= ['is_active']
    def save(self,commit=True):   
        user = super(UserForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.set_password(self.cleaned_data["password"])
        # user.address = self.cleaned_data['address']
        user.role_id = self.cleaned_data['role_id']
        # user.pincode = self.cleaned_data['pincode']
        if commit:
            user.save()
        return user

class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    new_password1 = forms.CharField(label=_("New password"),
                                    widget=forms.PasswordInput,
                                    help_text=password_validation.password_validators_help_text_html())
    new_password2 = forms.CharField(label=_("New password confirmation"),
                                    widget=forms.PasswordInput)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(SetPasswordForm, self).__init__(*args, **kwargs)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        password_validation.validate_password(password2, self.user)
        return password2

    def save(self, commit=True):
        password = self.cleaned_data["new_password1"]
        self.user.set_password(password)
        if commit:
            self.user.save()
        return self.user


class PasswordChangeForm(SetPasswordForm):
    """
    A form that lets a user change their password by entering their old
    password.
    """
    error_messages = dict(SetPasswordForm.error_messages, **{
        'password_incorrect': _("Your old password was entered incorrectly. "
                                "Please enter it again."),
    })
    old_password = forms.CharField(label=_("Old password"),
                                   widget=forms.PasswordInput)

    field_order = ['old_password', 'new_password1', 'new_password2']

    def clean_old_password(self):
        """
        Validates that the old_password field is correct.
        """
        print("old_password Change")
        old_password = self.cleaned_data["old_password"]
        if not self.user.check_password(old_password):
            print("old_password check_password")
            raise forms.ValidationError(
                self.error_messages['password_incorrect'],
                code='password_incorrect',
            )
        return old_password

class Tutor_Info_Form(forms.ModelForm):
      address = forms.CharField(required = True)
      image = forms.ImageField(required = True)
      class Meta:
          model = MyUser
          fields = ('first_name','last_name','address','gender','age','description','phone_no','image')


class Tutor_Qualifications_Form(forms.ModelForm):
      degree_name = forms.CharField(required = True)
      university_name = forms.CharField(required = True)
      percentage = forms.IntegerField(required=True)
      start_year = forms.DateField(required=True)
      complete_year = forms.DateField(required=True)
      class Meta:
          model = User_Qualification
          fields=('degree_name', 'university_name', 'percentage', 'start_year', 'complete_year')



class Tutor_Preference_Form(forms.ModelForm):
    tutor_detail = forms.CharField(required = True)
    teaching_exp = forms.CharField(required = True)
    TIME_SLOT_CHOICES = (
        ('M', 'Morning'),
        ('A', 'Afternoon'),
        ('E', 'Evening'),
    )
    prefer_time_slot = forms.ChoiceField(choices= TIME_SLOT_CHOICES)
    TUTION_TYPE_CHOICES = (
            ('O', 'ONE-ON-ONE'),
            ('G', 'GROUP'),
        )
    prefer_tution_type = forms.ChoiceField(choices= TUTION_TYPE_CHOICES,required = False)
    prefer_duration_session =  forms.CharField(required = True)
    Mon = forms.BooleanField(required=False, initial = False, label='Monday')
    Tue = forms.BooleanField(required=False, initial = False, label='Tuesday')
    Wed = forms.BooleanField(required=False, initial = False, label='Wednesday')
    Thu = forms.BooleanField(required=False, initial = False, label='Thursday')
    Fri = forms.BooleanField(required=False, initial = False, label='Friday')
    Sat = forms.BooleanField(required=False, initial = False, label='Saturday')
    Sun = forms.BooleanField(required=False, initial = False, label='Sunday')
    class Meta:
        model = UserTutorPreference
        fields=('tutor_detail', 'teaching_exp', 'prefer_time_slot', 'prefer_tution_type', 'prefer_duration_session', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun')         