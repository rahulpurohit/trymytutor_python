
# Create your views here.
from tutor.models import *
from django.shortcuts import render
from django.http import *
# from django.http import HttpResponse,HttpResponseRedirect, HttpResponseBadRequest, HttpResponseNotAllowed
from django.shortcuts import render , get_object_or_404 , redirect,render_to_response
from django.contrib.auth import authenticate, logout
from django.contrib.auth import login as auth_login
# from django.contrib.auth import login

from django.contrib.auth.forms import UserCreationForm
from tutor.forms import *

### For Forget Password
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes
from django.utils.encoding import force_text
from django.contrib.auth.tokens import default_token_generator
from django.core.urlresolvers import reverse
from django.shortcuts import resolve_url
from django.template import loader, RequestContext
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _
from django.http import QueryDict

from django.template.response import TemplateResponse
from django.contrib.auth.forms import (
    AuthenticationForm, PasswordChangeForm, PasswordResetForm, SetPasswordForm,
)
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.views.generic import FormView, DetailView, ListView





# Create your views here.
def base(request):
	return render(request,"base.html")
def faqs(request):
	return render(request,"faqs.html")



@login_required(login_url='/login/')
def main_page(request):
    print(request.user)
    return render(request,'main_page.html')

@login_required(login_url='/login/')
def search(request):
    print(request.user)
    return render(request,'search.html')

@login_required(login_url='/login/')
def payment(request):
    print(request.user)
    return render(request,'payment.html')

@login_required(login_url='/login/')
def profile_tutor(request):
    print(request.user)
    user=request.user
    userid=user.id
    try:
        qualifications = User_Qualification.objects.filter(user_id=userid)
    except User_Qualifications.DoesNotExist:
        qualifications = User_Qualification.objects.none
    try:
        preferences = UserTutorPreference.objects.get(user_id=userid)
    except UserTutorPreference.DoesNotExist:
        preferences = UserTutorPreference.objects.none
    context={
    "qualifications":qualifications,
    "preferences":preferences
    }

    return render(request,'profile_tutor.html', context)

@login_required(login_url='/login/')
def account_tutor(request):
    print(request.user)
    return render(request,'account_tutor.html')


@login_required(login_url='/login/')
def profile_student(request):
    print(request.user)
    return render(request,'profile_student.html')



@login_required(login_url='/login/')
def account(request):
    """
    for updating  tutor profile
    """

    user=request.user
    userid=user.id
    print (userid)
    tutor = get_object_or_404(MyUser, id=userid)
    form = Tutor_Info_Form(request.POST or None,  request.FILES or None, instance=tutor)
    if form.is_valid():
        instance=form.save(commit=False)
        instance.id=userid
        latlng = geocoder.google(form.cleaned_data['address'])
        lat = latlng.lat
        lng = latlng.lng
        print (lat)
        instance.lat = lat
        instance.lng = lng
        instance.save()

        return HttpResponseRedirect('/profile_tutor/')

    else:
        print("invalid form")
        print(form.errors)

    context = {
            "form":form,
            }
    return render(request, 'account.html', context)



@login_required(login_url='/login/')
def addQualifications(request):
    """
        for adding tutor qualifications
    """
    user=request.user
    userid=user.id
    print (userid)

    form = Tutor_Qualifications_Form(request.POST or None)
    if form.is_valid():
        instance=form.save(commit=False)
        instance.user_id=request.user
        instance.save()

        return HttpResponseRedirect('/profile_tutor/')

    else:
        print("invalid form")
        print(form.errors)

    context = {
            "form":form,
            }
    return render(request, 'profile_tutor.html#myModal2', context)



@login_required(login_url='/login/')
def delete_qualification(request, id=None):
    print(request.user)
    qualifications=User_Qualification.objects.filter(id=id)
    qualifications.delete()
    return HttpResponseRedirect('/profile_tutor/')




@login_required(login_url='/login/')
def addtutorPreference(request):
    """
    for adding tutor preferences
    """
    user=request.user
    userid=user.id
    print(userid)
    form = Tutor_Preference_Form(request.POST or None)
    if form.is_valid():
        instance=form.save(commit=False)
        instance.user_id=request.user
        instance.save()

        return HttpResponseRedirect('/profile_tutor/')

    else:
        print("invalid form")
        print(form.errors)
    context = {
            "form":form,
            }
    return render(request,'account.html', context)





def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/')

def register_page(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(username=form.cleaned_data['username'],password=form.cleaned_data['password1'],email=form.cleaned_data['email'])
            return HttpResponseRedirect('/')
    form = RegistrationForm()
    variables = RequestContext(request, {'form': form})
    return render_to_response('registration/register.html',variables)


# @login_required(login_url='/login/')
def register_tutor(request):
    print(request)
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            # user = User.objects.create_user(username=form.cleaned_data['username'],password=form.cleaned_data['password1'],email=form.cleaned_data['email'])
            return HttpResponseRedirect('/faqs')
        else:
            print(form.is_valid() )  #form contains data and errors
            print(form.errors)     
    else:
        form = UserForm()
    return render(request,'registration/register_tutor.html',{'form':form})
	      

def login_user(request):
    logout(request)
    email = password = ''
    if request.POST:
        print(request.POST)
        email = request.POST['email']
        password = request.POST['password']
        print(email)

        user = authenticate(email=email, password=password)
        print(user)
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                print("Success")
                return HttpResponseRedirect('/')

    return render(request, 'registration/login.html',{})


@login_required(login_url='/login/')
def student_dashboard(request):
    print(request.user)
    return render(request,'student_dashboard.html')


@login_required(login_url='/login/')
def student_dashboard_attendance(request):
    print(request.user)
    return render(request,'student_dashboard_attendance.html')



@login_required(login_url='/login/')
def student_dashboard_payment(request):
    print(request.user)
    return render(request,'student_dashboard_payment.html')    

