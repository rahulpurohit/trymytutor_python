from django.conf.urls import url
from django.contrib.auth import views as auth_views
from tutor import views

urlpatterns = [
##testing

	# url(r'^',views.base),
	url(r'^$', views.main_page),
	url(r'^faqs/$', views.faqs),
	# url(r'^user/(\w+)/$', user_page),
	# url(r'^login/$', 'django.contrib.auth.views.login')
	# url(r'^login/$', auth_views.login, name='login'),
	url(r'^login/$', views.login_user,name='login'),
	url(r'^logout/$', views.logout_page,name='logout'),
	url(r'^register/$', views.register_page,name='signup'),
	url(r'^signup_tutor/$', views.register_tutor,name='signup_tutor'),

	url(r'^search/$', views.search,name='search'),
	url(r'^payment/$', views.payment,name='payment'),
	url(r'^profile_tutor/$', views.profile_tutor,name='profile_tutor'),
	url(r'^account_tutor/$', views.account_tutor,name='account_tutor'),

	url(r'^profile_student/$', views.profile_student,name='profile_student'),

	url(r'^deletequalification/(?P<id>[0-9]+)/$', views.delete_qualification),
    url(r'^addqualifications/$', views.addQualifications),
	url(r'^addtutorPreference/$', views.addtutorPreference),
	url(r'^account/$', views.account,name='account'),
	url(r'^account#myModal/$', views.addtutorPreference,name='account'),



	url(r'^student_dashboard/$', views.student_dashboard,name='student_dashboard'),
	url(r'^student_dashboard_attendance/$', views.student_dashboard_attendance,name='student_dashboard_attendance'),
	url(r'^student_dashboard_payment/$', views.student_dashboard_payment,name='student_dashboard_payment'),



# 
]

