from tutor.models import *
from django.contrib import admin
from django.contrib.auth.models import Group

from django.contrib.auth.admin import UserAdmin
# Register your models here.
admin.site.register(Role)
admin.site.register(SubjectCategory)
admin.site.register(SubSubjectCategory)
admin.site.register(City)
admin.site.register(CityZone)
admin.site.register(Area)
admin.site.register(Board)
admin.site.register(MyUser)
admin.site.register(UserTutorPreference)
admin.site.register(University)
admin.site.register(SocialAuth)
admin.site.register(Rating)
admin.site.register(Static_Content)
# admin.site.register(MyUser, UserAdmin)


admin.site.unregister(Group)